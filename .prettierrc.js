module.exports = {
  printWidth: 80,
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  tabWidth: 2,
  // Override any other rules you want
};
