import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getSizesByProduct = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/product/${id}/sizes`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_SIZES, payload: res.data.data });
        } else {
          console.log(res);
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const getOneData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/product/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PRODUCT, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const createSize = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/product/${id}/size`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 201) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.CREATE_SIZE, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const updateSize = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .put(URL + `/api/size/${id}`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.UPDATE_SIZE, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const deleteSize = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete(URL + `/api/size/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.DELETE_SIZE, payload: id });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const getSizesCombo = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/product/${id}/sizes`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_SIZES_COMBO, payload: res.data.data });
        } else {
          console.log(res);
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};
