import { fetchError, fetchStart, fetchSuccess } from './Common';
import axios from 'axios';
import * as type from '../../@jumbo/constants/ActionTypes';

const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

export const getData = param => {
  const { pageSize, page, name } = param;
  const defaultUrl = URL + `/api/buyers?pageSize=${pageSize}&page=${page}`;
  const searchUrl = `${defaultUrl}&name=${name}`;
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(!name ? defaultUrl : searchUrl, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_CUSTOMERS, payload: res.data.data });
        } else {
          console.log(res);
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const getOneData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/buyers/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_CUSTOMER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const createData = payload => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/buyers`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        console.log(res);

        if (res.status === 201) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.CREATE_CUSTOMER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const updateData = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .put(URL + `/api/buyers/${id}`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.UPDATE_CUSTOMER, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const deleteData = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete(URL + `/api/buyers/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.DELETE_CUSTOMER, payload: id });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const getProductByCustomer = id => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .get(URL + `/api/buyers/${id}/sizes`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: type.GET_PRODUCT_CUST, payload: res.data.data });
        } else {
          console.log(res);
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const createProductByCustomer = (id, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .post(URL + `/api/buyers/${id}/size`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.CREATE_PRODUCT_CUST, payload: res.data.data });
        } else {
          dispatch(fetchError(res.data.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const updateProductByCustomer = (id, idSize, payload) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .put(URL + `/api/buyers/${id}/sizes/${idSize}`, payload, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.UPDATE_PRODUCT_CUST, payload: res.data.data });
        } else {
          console.log(res);
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};

export const deleteProductByCustomer = (id, idSize) => {
  return dispatch => {
    dispatch(fetchStart());
    axios
      .delete(URL + `/api/buyers/${id}/sizes/${idSize}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchSuccess(res.data.message));
          dispatch({ type: type.DELETE_PRODUCT_CUST, payload: idSize });
        } else {
          dispatch(fetchError(res.message.error));
        }
      })
      .catch(error => {
        dispatch(fetchError('There was something issue in responding server'));
      });
  };
};
