import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import Common from './Common';
import Products from './Products';
import Sizes from './Sizes';
import Auth from './Auth';
import Transactions from './Transactions';
import Customers from './Customers';

export default history =>
  combineReducers({
    router: connectRouter(history),
    common: Common,
    auth: Auth,
    productsReducer: Products,
    sizesReducer: Sizes,
    transactionsReducer: Transactions,
    customersReducer: Customers,
  });
