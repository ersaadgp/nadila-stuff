import { DELETE_ADMIN, GET_ADMINS } from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  admins: {},
  currentUser: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_ADMINS: {
      return {
        ...state,
        admins: action.payload,
      };
    }
    case DELETE_ADMIN: {
      return {
        ...state,
        admin: state.admins.filter(admin => admin.id !== action.payload),
      };
    }

    default:
      return state;
  }
};
