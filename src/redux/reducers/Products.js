import {
  GET_PRODUCTS,
  GET_PRODUCT,
  UPDATE_PRODUCT,
  CREATE_PRODUCT,
  DELETE_PRODUCT,
  CLEAR_PRODUCT,
  GET_PRODUCTS_COMBO,
} from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  products: [],
  product: {},
  combos: {},
  param: {},
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_PRODUCTS: {
      return {
        ...state,
        products: action.payload,
      };
    }
    case GET_PRODUCT: {
      return {
        ...state,
        product: action.payload,
      };
    }
    case CREATE_PRODUCT: {
      return {
        ...state,
        product: action.payload,
        // products: {
        //   data: [...state.products.data, action.payload],
        //   total: state.products.total + 1,
        // },
      };
    }
    case UPDATE_PRODUCT: {
      return {
        ...state,
        products: {
          data: state.products.data.map(product =>
            product.id === action.payload.id ? action.payload : product,
          ),
          total: state.products.total,
        },
      };
    }
    case DELETE_PRODUCT: {
      return {
        ...state,
        products: {
          data: state.products.data.filter(
            product => product.id !== action.payload,
          ),
          total: state.products.total - 1,
        },
      };
    }
    case GET_PRODUCTS_COMBO: {
      return {
        ...state,
        combos: action.payload.data.map(row => {
          return {
            name: `${row.uuid} - ${row.name}`,
            value: row.id,
          };
        }),
      };
    }
    case CLEAR_PRODUCT: {
      return {
        ...state,
        product: {},
      };
    }

    default:
      return state;
  }
};
