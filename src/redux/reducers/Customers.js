import {
  GET_CUSTOMERS,
  GET_CUSTOMER,
  UPDATE_CUSTOMER,
  CREATE_CUSTOMER,
  DELETE_CUSTOMER,
  CLEAR_CUSTOMER,
  GET_PRODUCT_CUST,
  DELETE_PRODUCT_CUST,
  CREATE_PRODUCT_CUST,
  UPDATE_PRODUCT_CUST,
} from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  customers: [],
  customer: {},
  products: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_CUSTOMERS: {
      return {
        ...state,
        customers: action.payload,
      };
    }
    case GET_CUSTOMER: {
      return {
        ...state,
        customer: action.payload,
      };
    }
    case CREATE_CUSTOMER: {
      return {
        ...state,
        customer: action.payload,
        // customers: {
        //   data: [...state.customers.data, action.payload],
        //   total: state.customers.total + 1,
        // },
      };
    }
    case UPDATE_CUSTOMER: {
      return {
        ...state,
        customer: action.payload,
      };
    }
    case DELETE_CUSTOMER: {
      return {
        ...state,
        customers: {
          data: state.customers.data.filter(
            customer => customer.id !== action.payload,
          ),
          total: state.customers.total - 1,
        },
      };
    }
    case GET_PRODUCT_CUST: {
      return {
        ...state,
        products: action.payload.data,
      };
    }
    case CREATE_PRODUCT_CUST: {
      return {
        ...state,
        products: [...state.products, action.payload],
      };
    }
    case UPDATE_PRODUCT_CUST: {
      return {
        ...state,
        products: state.products.map(row =>
          row.size_id === action.payload.size_id ? action.payload : row,
        ),
      };
    }
    case DELETE_PRODUCT_CUST: {
      return {
        ...state,
        products: state.products.filter(row => row.size_id != action.payload),
      };
    }
    case CLEAR_CUSTOMER: {
      return {
        ...state,
        customer: {},
      };
    }

    default:
      return state;
  }
};
