import { GET_TRANSACTIONS } from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  transactions: [],
  param: {},
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_TRANSACTIONS: {
      return {
        ...state,
        transactions: action.payload,
      };
    }
    case 'SET_PARAM': {
      return {
        ...state,
        param: action.payload,
      };
    }
    default:
      return state;
  }
};
