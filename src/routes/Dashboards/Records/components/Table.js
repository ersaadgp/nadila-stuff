import React from 'react';

const TablePrint = ({ data, ref }) => {
  return (
    <table ref={ref}>
      <thead>
        <tr>
          <th>Code</th>
          <th>Product Name</th>
          <th>Size</th>
          <th>Amount</th>
          <th>Total Price</th>
          <th>Actual Price</th>
          <th>Profit</th>
        </tr>
      </thead>
      <tbody>
        {data?.map((row, i) => (
          <tr key={i}>
            <td>{row.product_code}</td>
            <td>{row.product_name}</td>
            <td>{row.size}</td>
            <td>{row.amount}</td>
            <td>{row.total_price}</td>
            <td>{row.total_actual_price}</td>
            <td>{row.total_actual_price}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TablePrint;
