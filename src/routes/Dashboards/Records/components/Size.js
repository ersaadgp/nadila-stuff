import React, { useState } from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import { useDispatch, useSelector } from 'react-redux';
import { Typography, Button, Box, Paper, Grid } from '@material-ui/core';
import { Remove, Add, Save } from '@material-ui/icons';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import Modal from './ModalQR';

const useStyles = makeStyles(theme => ({
  content: {
    padding: 15,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}));

const Attachement = props => {
  const {
    data,
    index,
    handleChangeSizes,
    action,
    setSizes,
    sizes,
    add,
    name,
    color,
  } = props;

  const { users } = useSelector(({ userReducer }) => userReducer);
  const classes = useStyles();
  const [stock, setStock] = useState(data?.stock || 0);
  const [size, setSize] = useState(data?.size || 0);
  const [isLoading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const isUpdate = action === 'update';
  const isCreate = action === 'add';
  const isDetail = action === 'detail';

  const handleChange = async type => {
    if (type === 'minus') setStock(stock - 1);
    else setStock(stock + 1);

    setLoading(true);
    await handleChangeSizes(stock, index);
    setLoading(false);
  };

  const handleAdd = () => {
    setSizes([...sizes, { size: parseInt(size), stock: parseInt(stock) }]);
    setSize(0);
    setStock(0);
  };

  return (
    <>
      <Paper
        variant="outlined"
        className={isDetail ? 'pointer-float' : ''}
        {...(isDetail ? { onClick: () => setOpen(true) } : {})}>
        <Box className={classes.content}>
          {add ? (
            <Grid container spacing={3}>
              <Grid item xs={5}>
                <AppTextInput
                  label="Size"
                  type="number"
                  value={size}
                  onChange={e => setSize(e.target.value)}
                />
              </Grid>
              <Grid item xs={5}>
                <AppTextInput
                  value={stock}
                  label="Stock"
                  onChange={e => setStock(e.target.value)}
                  type="number"
                />
              </Grid>
              <Grid item xs={1}>
                <Button
                  disabled={!size || !stock}
                  onClick={handleAdd}
                  variant="outlined"
                  color="primary"
                  className="mt-3 btn-size">
                  <Add />
                </Button>
              </Grid>
            </Grid>
          ) : (
            <>
              <Typography variant="subtitle2">
                {open ? 'Loading' : `Size ${data?.size}`}
              </Typography>
              <div>
                <Button
                  variant="outlined"
                  color="primary"
                  className="btn-size"
                  disabled={isDetail}
                  onClick={() => handleChange('minus')}>
                  <Remove />
                </Button>
                <Typography variant="caption" className="ml-3 mr-3">
                  {data?.stock}
                </Typography>
                <Button
                  disabled={isDetail}
                  variant="outlined"
                  color="primary"
                  className="btn-size"
                  onClick={() => handleChange('plus')}>
                  <Add />
                </Button>
              </div>
            </>
          )}
        </Box>
        <Modal
          open={open}
          setOpen={setOpen}
          data={data}
          name={name}
          color={color}
        />
      </Paper>
    </>
  );
};

export default Attachement;
