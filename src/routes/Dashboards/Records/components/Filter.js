import React, { useState } from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { TextField, Grid, Button, Box } from '@material-ui/core';
import AppTextInput from '../../../../@jumbo/components/Common/formElements/AppTextInput';
import AppSelectBox from '@jumbo/components/Common/formElements/AppSelectBox';
import { DatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

export default function SimpleAccordion() {
  const dispatch = useDispatch();
  const [selectedDate, handleDateChange] = useState(new Date());
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [type, setType] = useState(0);

  const handleSubmit = () => {
    const payload = {
      ...(type === 1
        ? {
            start_date: moment(startDate).format('yyyy-MM-DD'),
            end_date: moment(endDate).format('yyyy-MM-DD'),
          }
        : { date: moment(selectedDate).format('yyyy-MM-DD') }),
    };
    dispatch({ type: 'SET_PARAM', payload });
  };
  return (
    <div className="mb-3">
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header">
          <Typography className="ml-3">Filter</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container className="ml-3 mr-3 mb-5" spacing={3}>
            <Grid item md={4}>
              <AppSelectBox
                variant="outlined"
                fullWidth
                label="Type"
                valueKey="value"
                labelKey="label"
                data={[
                  { label: 'Specific', value: 0 },
                  { label: 'Range', value: 1 },
                ]}
                value={type}
                onChange={e => setType(e.target.value)}
                size="small"
              />
            </Grid>
            {type === 1 ? (
              <>
                <Grid item md={4}>
                  <DatePicker
                    inputVariant="outlined"
                    fullWidth
                    autoOk
                    label="Start Date"
                    clearable
                    disableFuture
                    disableToolbar
                    format="DD/MM/yyyy"
                    value={startDate}
                    size="small"
                    onChange={setStartDate}
                  />
                </Grid>
                <Grid item md={4}>
                  <DatePicker
                    inputVariant="outlined"
                    fullWidth
                    autoOk
                    label="End Date"
                    clearable
                    format="DD/MM/yyyy"
                    disableFuture
                    value={endDate}
                    disableToolbar
                    size="small"
                    onChange={setEndDate}
                  />
                </Grid>
              </>
            ) : (
              <Grid item md={4}>
                <DatePicker
                  inputVariant="outlined"
                  fullWidth
                  autoOk
                  format="DD/MM/yyyy"
                  label="Date"
                  clearable
                  disableFuture
                  value={selectedDate}
                  disableToolbar
                  size="small"
                  onChange={handleDateChange}
                />
              </Grid>
            )}
            <Grid item md={12}>
              <Box display="flex" justifyContent="flex-end">
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleSubmit}>
                  Apply
                </Button>
              </Box>
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
