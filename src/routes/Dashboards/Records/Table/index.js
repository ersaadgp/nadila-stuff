import React, { useState, useEffect, useRef } from 'react';
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import CmtSearch from '../../../../@coremat/CmtSearch';
import RecentTable from './RecentTable';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Button } from '@material-ui/core';

import makeStyles from '@material-ui/core/styles/makeStyles';
import { useDispatch, useSelector } from 'react-redux';
import { getData } from '../../../../redux/actions/Transactions';
import Pagination from './TableFooter';
import { useHistory } from 'react-router';
import { Print } from '@material-ui/icons';
import ReactToPrint from 'react-to-print';

const useStyles = makeStyles(theme => ({
  cardContentRoot: {
    padding: '0 !important',
  },
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const style = {
  width: '90%',
  margin: '15px auto',
  color: 'black',
};

const RecentOrders = () => {
  const componentRef = useRef();
  const { transactions, param } = useSelector(
    ({ transactionsReducer }) => transactionsReducer,
  );
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [value, setValue] = useState(null);
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalActualPrice, setTotalActualPrice] = useState(0);
  const [totalProfit, setTotalProfit] = useState(0);
  const [amounts, setAmounts] = useState(0);
  const [periode, setPeriode] = useState('All');

  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const fetchData = (rowsPerPage, page, value) => {
    const params = {
      pageSize: rowsPerPage,
      page: page + 1,
      name: value,
      dateParam: param,
    };
    dispatch(getData(params));
  };

  useEffect(() => {
    fetchData(rowsPerPage, page, value);
  }, [rowsPerPage, page, value, param]);

  useEffect(() => {
    generatePrice();
  }, [transactions]);

  const handleClick = () => {
    history.push('/dashboard/products/add');
  };

  const generatePrice = () => {
    if (transactions?.data) {
      let total = 0;
      let actual = 0;
      let amount = 0;
      for (const tx of transactions?.data) {
        let newTotal = tx.total_price.replace('Rp.', '');
        newTotal = newTotal.replace('.', '');
        newTotal = parseInt(newTotal);
        total = total + newTotal;

        let newActual = tx.actual_price.replace('Rp.', '');
        newActual = newActual.replace('.', '');
        newActual = parseInt(newActual);
        actual = actual + newActual;

        amount = amount + tx.amount;
      }
      setTotalActualPrice(
        'Rp ' + actual.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'),
      );
      setTotalPrice(
        'Rp ' + total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'),
      );
      const profit = total - actual;
      setTotalProfit(
        'Rp ' + profit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'),
      );
      setAmounts(amount);

      if (param?.date || param?.start_date) {
        let prd = param?.date
          ? param?.date
          : `${param?.startDate} - ${param?.endDate}`;
        setPeriode(prd);
      }
    }
  };

  return (
    <>
      <div className="print">
        <div ref={componentRef}>
          <div className="title">
            <h3>LAPORAN PENJUALAN</h3>
            <p>Periode: {periode}</p>
          </div>
          <table className="table" style={style}>
            <thead>
              <tr className="tr">
                <th>Code</th>
                <th>Product Name</th>
                <th>Size</th>
                <th>Amount</th>
                <th>Total Price</th>
                <th>Actual Price</th>
              </tr>
            </thead>
            <tbody>
              {transactions?.data?.map((row, i) => (
                <tr key={i} className="tr">
                  <td style={{ color: 'black' }}>{row.product_code}</td>
                  <td>{row.product_name}</td>
                  <td>{row.size}</td>
                  <td>{row.amount} Pcs</td>
                  <td>{row.total_price}</td>
                  <td>{row.total_actual_price}</td>
                </tr>
              ))}
              <tr>
                <td colSpan="3">Total Sell</td>
                <td>{amounts} Pcs</td>
                <td>{totalPrice}</td>
                <td>{totalActualPrice}</td>
              </tr>
              <tr>
                <td colSpan="5">Total Profit</td>
                <td>{totalProfit}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <CmtCard>
        <CmtCardHeader
          className="pt-4"
          title="List Record"
          titleProps={{
            variant: 'h4',
            component: 'div',
            className: classes.titleRoot,
          }}>
          <ReactToPrint
            trigger={() => (
              <Button
                color="primary"
                variant="contained"
                className="ml-3"
                onClick={handleClick}>
                <Print />
              </Button>
            )}
            content={() => componentRef.current}
            // onAfterPrint={() => setOpen(false)}
          />
        </CmtCardHeader>
        <CmtCardContent className={classes.cardContentRoot}>
          {transactions?.data && (
            <PerfectScrollbar className={classes.scrollbarRoot}>
              <RecentTable tableData={transactions?.data} />
            </PerfectScrollbar>
          )}
        </CmtCardContent>
      </CmtCard>
    </>
  );
};

export default RecentOrders;
