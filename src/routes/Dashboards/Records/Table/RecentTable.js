import React from 'react';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableHeading from './TableHeading';
import TableItem from './TableItem';
import Box from '@material-ui/core/Box';

const RecentTable = ({ tableData }) => {
  const header = [
    'Photo',
    'Code',
    'Product Name',
    'Size',
    'Amount',
    'Total Price',
    'Actual Price',
  ];

  return (
    <Box className="Cmt-table-responsive">
      <Table>
        <TableHead>
          <TableHeading header={header} />
        </TableHead>
        <TableBody>
          {tableData.map((row, index) => (
            <TableItem row={row} key={index} />
          ))}
        </TableBody>
      </Table>
    </Box>
  );
};

export default RecentTable;
