import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(theme => ({
  tableCellRoot: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 0,
    paddingBottom: 12,
    fontSize: 12,
    letterSpacing: 0.4,
    color: theme.palette.common.dark,
    borderBottom: '0 none',
    '&:first-child': {
      paddingLeft: 24,
    },
    '&:last-child': {
      textAlign: 'left',
      paddingRight: 24,
    },
  },
}));

const TableHeading = ({ header }) => {
  const classes = useStyles();
  return (
    <TableRow>
      {header.map((row, i) => {
        return (
          <TableCell key={i} className={classes.tableCellRoot}>
            {row}
          </TableCell>
        );
      })}
    </TableRow>
  );
};

export default TableHeading;
