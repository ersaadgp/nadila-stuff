import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';

// component
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Typography, Grid, Box, Button } from '@material-ui/core';
import Product from '../components/Product';
import ModalCreate from '../components/ModalCreate';
import Modal from '../components/Modal';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import { Edit, Save, Delete, Add, ArrowBack, Print } from '@material-ui/icons';

// state
import { useDispatch, useSelector } from 'react-redux';
import {
  getOneData,
  deleteData,
  createData,
  updateData,
  getProductByCustomer,
} from 'redux/actions/Customers';

const def_img =
  'https://assets.adidas.com/images/w_600,f_auto,q_auto/4e894c2b76dd4c8e9013aafc016047af_9366/Superstar_Shoes_White_FV3284_01_standard.jpg';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const RecentOrders = () => {
  const { customer, products: prods } = useSelector(
    ({ customersReducer }) => customersReducer,
  );

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { id, action } = useParams();

  const isUpdate = action === 'update';
  const isCreate = action === 'add';
  const isDetail = action === 'detail';

  const [customerv, setCustomerv] = useState('');
  const [address, setAddress] = useState('');
  const [resi, setResi] = useState('');
  const [total, setTotal] = useState(0);
  const [phone, setPhone] = useState(0);
  const [products, setProducts] = useState([]);
  const [isDisabled, setDisabled] = useState(true);
  const [open, setOpen] = useState(false);
  const [modalCreate, setModalCreate] = useState(false);

  useEffect(() => {
    if (customer?.id) dispatch(getProductByCustomer(customer?.id));
  }, [customer?.id]);

  useEffect(() => {
    if (id) dispatch(getOneData(id));
    else {
      setCustomerv('');
      setAddress('');
      setTotal(0);
      setProducts([]);
    }
  }, [id]);

  useEffect(() => {
    if (customer?.id && !isCreate) {
      setCustomerv(customer.name);
      setAddress(customer.address);
      setResi(customer.no_resi);
      setPhone(customer.phone);
    }
  }, [customer]);

  useEffect(() => {
    if (customer?.id) {
      setProducts(prods);
    }
  }, [customer, prods]);

  const data = [
    {
      label: 'No. Resi',
      value: resi,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Customer Name',
      value: customerv,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Phone',
      value: phone,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Address',
      value: address,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Total',
      value: total,
      isDisabled: true,
      type: 'number',
    },
  ];

  const handleChange = (value, i) => {
    if (i === 1) setResi(value);
    if (i === 2) setCustomerv(value);
    if (i === 3) setPhone(value);
    if (i === 4) setAddress(value);
  };

  const handleSubmit = () => {
    const payload = {
      name: customerv,
      no_resi: resi,
      phone: phone,
      address: address,
      total_price: total,
    };
    if (!customer?.id) {
      dispatch(createData(payload));
    } else {
      dispatch(updateData(customer?.id, payload));
      history.push(`/dashboard/customers/detail/${customer?.id}`);
    }
  };

  const handleDelete = () => {
    dispatch(deleteData(id));
    history.push('/dashboard/customers');
  };

  useEffect(() => {
    if (address && resi && customer && phone) setDisabled(false);
  }, [address, resi, products, customer]);

  useEffect(() => {
    if (prods?.length && customer?.id) {
      let total = 0;
      for (const product of prods) {
        console.log(product.product_price * product.quantity);
        total += product.product_price * product.quantity;
      }
      setTotal(total);
    }
  }, [products]);

  return (
    <>
      <CmtCard className="mb-3">
        <CmtCardHeader
          className="pt-4"
          title="Product Detail"
          titleProps={{
            variant: 'h4',
            component: 'div',
            className: classes.titleRoot,
          }}></CmtCardHeader>
        <CmtCardContent className={classes.cardContentRoot}>
          {customer && (
            <Grid container spacing={5}>
              {data.map((row, i) => (
                <Grid
                  item
                  lg={10}
                  xl={row.label === 'Address' ? 12 : 4}
                  key={i}>
                  <AppTextInput
                    variant="outlined"
                    label={row.label}
                    value={row.value}
                    onChange={e => handleChange(e.target.value, i + 1)}
                    disabled={row.isDisabled}
                    type={row.type || 'text'}
                  />
                </Grid>
              ))}
            </Grid>
          )}
        </CmtCardContent>
      </CmtCard>
      {customer?.id && (
        <CmtCard>
          <CmtCardHeader
            className="pt-4"
            title="List Customer's Products"
            titleProps={{
              variant: 'h4',
              component: 'div',
              className: classes.titleRoot,
            }}>
            {!isDetail && (
              <Button
                onClick={() => setModalCreate(true)}
                color="primary"
                variant="contained">
                <Add />
              </Button>
            )}
          </CmtCardHeader>
          <CmtCardContent className={classes.cardContentRoot}>
            <Grid container spacing={3} className="sizes">
              {products &&
                products.map((row, i) => (
                  <Grid item xl={4} lg={6} key={i}>
                    <Product data={row} index={i} action={action} id={id} />
                  </Grid>
                ))}
            </Grid>
          </CmtCardContent>
        </CmtCard>
      )}
      <Box display="flex" justifyContent="flex-end" className="mt-3">
        {isUpdate && (
          <Button
            color="secondary"
            variant="outlined"
            className="mr-3"
            onClick={() => history.push(`/dashboard/customers/detail/${id}`)}>
            Cancel
          </Button>
        )}
        {isDetail && (
          <>
            <Button
              color="default"
              variant="outlined"
              className="mr-3"
              onClick={() => history.push(`/dashboard/customers`)}>
              <ArrowBack className="mr-2" /> Back
            </Button>
            <Button
              className="mr-3"
              color="secondary"
              variant="outlined"
              onClick={() => setOpen(true)}>
              <Delete className="mr-2" /> Delete
            </Button>
            <Button
              className="mr-3"
              color="primary"
              variant="outlined"
              onClick={() => history.push(`/dashboard/customers/print/${id}`)}>
              <Print className="mr-2" /> Print
            </Button>
            <Button
              color="primary"
              variant="contained"
              onClick={() => history.push(`/dashboard/customers/update/${id}`)}>
              <Edit className="mr-2" /> Edit
            </Button>
          </>
        )}
        {!isDetail && (
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
            disabled={isDisabled}>
            <Save className="mr-2" /> Save
          </Button>
        )}
      </Box>
      <Modal open={open} setOpen={setOpen} handleDelete={handleDelete} />
      <ModalCreate
        open={modalCreate}
        setOpen={setModalCreate}
        id={customer?.id}
        isCreate={true}
      />
    </>
  );
};

export default RecentOrders;
