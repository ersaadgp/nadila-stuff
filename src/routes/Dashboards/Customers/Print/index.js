import React, { useState, useEffect, useRef } from 'react';
import { useHistory, useParams } from 'react-router';
import QRCode from 'react-qr-code';

// component
import { Typography, Grid, Box, Button } from '@material-ui/core';
import { Edit, Save, Delete, Add, ArrowBack, Print } from '@material-ui/icons';
import ReactToPrint from 'react-to-print';

// state
import { useDispatch, useSelector } from 'react-redux';
import { getOneData, getProductByCustomer } from 'redux/actions/Customers';

const RecentOrders = () => {
  const componentRef = useRef();

  const { customer, products: prods } = useSelector(
    ({ customersReducer }) => customersReducer,
  );

  const history = useHistory();
  const dispatch = useDispatch();
  const { id } = useParams();

  const [customerv, setCustomerv] = useState('');
  const [address, setAddress] = useState('');
  const [resi, setResi] = useState('');
  const [total, setTotal] = useState(0);
  const [phone, setPhone] = useState(0);
  const [products, setProducts] = useState([]);
  const [isDisabled, setDisabled] = useState(true);

  useEffect(() => {
    if (customer?.id) dispatch(getProductByCustomer(customer?.id));
  }, [customer?.id]);

  useEffect(() => {
    if (id) dispatch(getOneData(id));
    else {
      setCustomerv('');
      setAddress('');
      setTotal(0);
      setProducts([]);
    }
  }, [id]);

  useEffect(() => {
    if (customer?.id) {
      setCustomerv(customer.name);
      setAddress(customer.address);
      setResi(customer.no_resi);
      setPhone(customer.phone);
    }
  }, [customer]);

  useEffect(() => {
    if (customer?.id) {
      setProducts(prods);
    }
  }, [customer, prods]);

  const data = [
    { label: 'No. Resi', value: resi },
    { label: 'Phone', value: phone },
    {
      label: 'Total',
      value: 'Rp ' + total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'),
    },
    { label: 'Address', value: address },
  ];

  useEffect(() => {
    if (address && resi && customer && phone) setDisabled(false);
  }, [address, resi, products, customer]);

  useEffect(() => {
    if (prods?.length) {
      let total = 0;
      for (const product of prods) {
        console.log(product.product_price * product.quantity);
        total += product.product_price * product.quantity;
      }
      setTotal(total);
    }
  }, [products]);

  return (
    <>
      {customer && (
        <div ref={componentRef} className="print-customer">
          <Typography variant="h1" className="mt-3" component="div">
            Customer {customerv}
          </Typography>
          <div className="info-cust-container">
            {data.map(row => (
              <div key={row.label} className="info-cust">
                <p className="label">{row.label}</p>
                <p className="content">{row.value}</p>
              </div>
            ))}
          </div>
          <Typography variant="h4" className="mt-3" component="div">
            List Product
          </Typography>
          <div className="qr-container">
            {products &&
              products.map((row, i) => (
                <div className="qr-item">
                  <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <QRCode value={`${row.size_id}`} key={i} size="150" />
                  </div>
                  <Typography
                    variant="subtitle2"
                    align="center"
                    component="div"
                    className="mt-3">
                    {row.product_name}
                  </Typography>
                  <Typography variant="caption" align="center" component="div">
                    Size : {row.size} | Quantity : {row.quantity}
                  </Typography>
                </div>
              ))}
          </div>
        </div>
      )}

      <Box display="flex" justifyContent="flex-end" className="mt-3">
        <Button
          className="mr-3"
          color="default"
          variant="outlined"
          onClick={() => history.push(`/dashboard/customers/detail/${id}`)}>
          <ArrowBack className="mr-2" /> Cancel
        </Button>
        <ReactToPrint
          trigger={() => (
            <Button color="primary" variant="contained">
              <Print className="mr-2" /> Print
            </Button>
          )}
          content={() => componentRef.current}
        />
      </Box>
    </>
  );
};

export default RecentOrders;
