import React, { useState } from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import { useDispatch, useSelector } from 'react-redux';
import { Typography, Button, Box, Paper, Grid } from '@material-ui/core';
import { Delete, Add, Edit, Visibility } from '@material-ui/icons';
import ModalCreate from './ModalCreate';
import { deleteProductByCustomer } from 'redux/actions/Customers';

const useStyles = makeStyles(theme => ({
  content: {
    padding: 15,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}));

const ProductCards = props => {
  const { data, action, id } = props;

  const dispatch = useDispatch();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const isDetail = action === 'detail';

  const handleDelete = () => {
    dispatch(deleteProductByCustomer(id, data?.size_id));
  };

  return (
    <>
      <Paper variant="outlined">
        <Box className={classes.content}>
          <>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Typography variant="subtitle2" align="center">
                  {data?.product_name} | {data?.size}
                </Typography>
              </Grid>
            </Grid>
            <Button
              variant="outlined"
              color="primary"
              className="btn-size mr-1"
              onClick={() => setOpen(true)}>
              <Visibility />
            </Button>
            {!isDetail && (
              <Button
                variant="outlined"
                color="secondary"
                className="btn-size"
                onClick={() => handleDelete()}>
                <Delete />
              </Button>
            )}
          </>
        </Box>
      </Paper>
      <ModalCreate
        open={open}
        setOpen={setOpen}
        data={data}
        isDetail={isDetail}
        id={id}
      />
    </>
  );
};

export default ProductCards;
