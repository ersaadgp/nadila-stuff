import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch, useSelector } from 'react-redux';
import { getDataCombo, getOneData } from 'redux/actions/Products';
import { getSizesCombo } from 'redux/actions/Sizes';

function sleep(delay = 0) {
  return new Promise(resolve => {
    setTimeout(resolve, delay);
  });
}

export default function Asynchronous({ name, isDetail, id }) {
  const dispatch = useDispatch();
  const { combos } = useSelector(({ productsReducer }) => productsReducer);
  const { loading } = useSelector(({ common }) => common);
  const [value, setValue] = useState({ value: id });
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    dispatch(getDataCombo({ page: 1, name: inputValue }));
  }, [inputValue]);

  useEffect(() => {
    if (value?.value) {
      dispatch(getOneData(value?.value));
      dispatch(getSizesCombo(value?.value));
    }
  }, [value]);

  useEffect(() => {
    if (name) setInputValue(name);
  }, [name]);

  return (
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(e, newInputValue) => {
        setInputValue(newInputValue);
      }}
      getOptionLabel={combos => combos.name || ''}
      id="controllable-states-demo"
      options={combos}
      loading={loading}
      disabled={isDetail}
      renderInput={params => (
        <TextField
          {...params}
          label="Product Name"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
