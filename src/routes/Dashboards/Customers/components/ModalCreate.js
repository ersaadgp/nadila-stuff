import React, { useEffect, useState } from 'react';
import { Grid, Button, Box, Typography } from '@material-ui/core';
import Fade from '@material-ui/core/Fade';
import Modal from '@material-ui/core/Modal';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import Autocomplete from './Autocomplete';
import { useDispatch, useSelector } from 'react-redux';
import AppSelectBox from '@jumbo/components/Common/formElements/AppSelectBox';
import {
  createProductByCustomer,
  updateProductByCustomer,
} from 'redux/actions/Customers';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
  textAlign: 'center',
  borderRadius: '8px',
};

const btnStyle = {
  width: '100%',
  zIndex: 99,
};

export default function BasicModal({
  open,
  setOpen,
  data,
  isDetail,
  id,
  isCreate,
}) {
  const dispatch = useDispatch();
  const { product } = useSelector(({ productsReducer }) => productsReducer);
  const { sizesCombo } = useSelector(({ sizesReducer }) => sizesReducer);
  const [productName, setProductName] = useState('');
  const [size, setSize] = useState(0);
  const [qty, setQty] = useState(0);
  const [color, setColor] = useState('');
  const [price, setPrice] = useState('');

  const handleClose = () => setOpen(false);

  useEffect(() => {
    setSize(0);
    setQty(0);
    setColor('');
    setPrice('');
  }, [isCreate]);

  useEffect(() => {
    if (product?.id && isCreate) {
      setPrice(product.price);
      setColor(product.color);
    }
  }, [product]);

  useEffect(() => {
    if (sizesCombo[0]?.value) setSize(sizesCombo[0]?.value);
  }, [sizesCombo]);

  useEffect(() => {
    if (data && !isCreate) {
      setSize(data.size_id);
      setQty(data.quantity);
      setColor(data.product_color);
      setPrice(data.product_price);
    }
  }, [data]);

  const handleAdd = () => {
    const payload = {
      size_id: size,
      quantity: qty,
    };
    isCreate
      ? dispatch(createProductByCustomer(id, payload))
      : dispatch(updateProductByCustomer(id, data.size_id, payload));
    handleClose();
  };

  return (
    <div>
      <Modal open={open} onClose={handleClose}>
        <Fade in={open}>
          <Box sx={style}>
            <Typography variant="h4" align="left" className="mb-3">
              {isDetail ? 'Detail' : 'Form'} Product
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Autocomplete
                  name={data?.product_name}
                  id={data?.product_id}
                  isDetail={isDetail}
                />
              </Grid>
              <Grid item xs={6}>
                <AppTextInput
                  disabled
                  variant="outlined"
                  value={color}
                  label="Color"
                />
              </Grid>
              <Grid item xs={6}>
                <AppTextInput
                  disabled
                  variant="outlined"
                  value={price}
                  label="Price"
                />
              </Grid>
              <Grid item xs={6}>
                <AppSelectBox
                  disabled={isDetail}
                  variant="outlined"
                  value={size}
                  valueKey="value"
                  labelKey="label"
                  data={sizesCombo}
                  label="Size"
                  onChange={e => setSize(e.target.value)}
                />
              </Grid>
              <Grid item xs={6}>
                <AppTextInput
                  disabled={isDetail}
                  variant="outlined"
                  value={qty}
                  label="Qty"
                  onChange={e => setQty(e.target.value)}
                  type="number"
                />
              </Grid>
              {!isDetail && (
                <>
                  <Grid item xs={6}>
                    <Button
                      style={btnStyle}
                      onClick={handleClose}
                      variant="outlined"
                      color="secondary">
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      style={btnStyle}
                      onClick={handleAdd}
                      variant="contained"
                      color="primary">
                      Save
                    </Button>
                  </Grid>
                </>
              )}
            </Grid>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
}
