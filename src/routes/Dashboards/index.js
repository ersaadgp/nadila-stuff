import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import PageLoader from '../../@jumbo/components/PageComponents/PageLoader';

const Dashboards = ({ match }) => {
  const requestedUrl = match.url.replace(/\/$/, '');
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Redirect
          exact
          from={`${requestedUrl}/`}
          to={`${requestedUrl}/products`}
        />

        {/* Products */}
        <Route
          exact
          path={`${requestedUrl}/products`}
          component={lazy(() => import('./Products'))}
        />
        <Route
          exact
          path={`${requestedUrl}/products/:action`}
          component={lazy(() => import('./Products'))}
        />
        <Route
          exact
          path={`${requestedUrl}/products/:action/:id`}
          component={lazy(() => import('./Products'))}
        />

        {/* Customers */}
        <Route
          exact
          path={`${requestedUrl}/customers`}
          component={lazy(() => import('./Customers'))}
        />
        <Route
          exact
          path={`${requestedUrl}/customers/:action`}
          component={lazy(() => import('./Customers'))}
        />
        <Route
          exact
          path={`${requestedUrl}/customers/:action/:id`}
          component={lazy(() => import('./Customers'))}
        />

        {/* Records */}
        <Route
          exact
          path={`${requestedUrl}/records`}
          component={lazy(() => import('./Records'))}
        />
        <Route
          exact
          path={`${requestedUrl}/records/:action`}
          component={lazy(() => import('./Records'))}
        />
        <Route
          exact
          path={`${requestedUrl}/records/:action/:id`}
          component={lazy(() => import('./Records'))}
        />

        {/* <Route component={lazy(() => import('../ExtraPages/404'))} /> */}
      </Switch>
    </Suspense>
  );
};

export default Dashboards;
