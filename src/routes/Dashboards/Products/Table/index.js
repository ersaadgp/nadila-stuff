import React, { useState, useEffect } from 'react';
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import CmtSearch from '../../../../@coremat/CmtSearch';
import RecentTable from './RecentTable';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Button } from '@material-ui/core';

import makeStyles from '@material-ui/core/styles/makeStyles';
import { useDispatch, useSelector } from 'react-redux';
import { getData } from '../../../../redux/actions/Products';
import Pagination from './TableFooter';
import { useHistory } from 'react-router';
import { Add } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  cardContentRoot: {
    padding: '0 !important',
  },
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const RecentOrders = () => {
  const { products } = useSelector(({ productsReducer }) => productsReducer);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [value, setValue] = useState('');

  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const fetchData = (rowsPerPage, page, value) => {
    const param = { pageSize: rowsPerPage, page: page + 1, name: value };
    dispatch(getData(param));
  };

  useEffect(() => {
    fetchData(rowsPerPage, page, value);
  }, [rowsPerPage, page, value]);

  useEffect(() => {
    dispatch({ type: 'CLEAR_PRODUCT' });
  }, []);

  const handleClick = () => {
    history.push('/dashboard/products/add');
  };

  return (
    <CmtCard>
      <CmtCardHeader
        className="pt-4"
        title="List Product"
        titleProps={{
          variant: 'h4',
          component: 'div',
          className: classes.titleRoot,
        }}>
        <CmtSearch
          id="search"
          border={true}
          onlyIcon={false}
          iconPosition="right"
          align="right"
          placeholder="Search Keywords"
          value={value}
          onChange={e => setValue(e.target.value)}
        />
        <Button
          color="primary"
          variant="contained"
          className="ml-3"
          onClick={handleClick}>
          <Add />
        </Button>
      </CmtCardHeader>
      <CmtCardContent className={classes.cardContentRoot}>
        {products?.data && (
          <>
            <PerfectScrollbar className={classes.scrollbarRoot}>
              <RecentTable tableData={products?.data} />
            </PerfectScrollbar>
            <Pagination
              rows={products?.data}
              total={products?.total}
              page={page}
              setPage={setPage}
              rowsPerPage={rowsPerPage}
              setRowsPerPage={setRowsPerPage}
            />
          </>
        )}
      </CmtCardContent>
    </CmtCard>
  );
};

export default RecentOrders;
