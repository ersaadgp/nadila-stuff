import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import { getBase64 } from '../components/helper';
import { useDropzone } from 'react-dropzone';

// component
import CmtCard from '../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Typography, Grid, Box, Button } from '@material-ui/core';
import Size from '../components/Size';
import Modal from '../components/Modal';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import { Edit, Save, Delete, ArrowBack } from '@material-ui/icons';

// state
import { useDispatch, useSelector } from 'react-redux';
import {
  getOneData,
  deleteData,
  createData,
  updateData,
} from 'redux/actions/Products';
import { getSizesByProduct } from 'redux/actions/Sizes';
import { products } from '@fake-db';

const def_img =
  'https://assets.adidas.com/images/w_600,f_auto,q_auto/4e894c2b76dd4c8e9013aafc016047af_9366/Superstar_Shoes_White_FV3284_01_standard.jpg';

const useStyles = makeStyles(theme => ({
  titleRoot: {
    letterSpacing: 0.15,
  },
  scrollbarRoot: {
    maxHeight: '50vh',
  },
  badgeRoot: {
    color: theme.palette.common.white,
    borderRadius: 30,
    fontSize: 12,
    padding: '2px 10px',
    display: 'inline-block',
  },
}));

const RecentOrders = () => {
  const { product } = useSelector(({ productsReducer }) => productsReducer);
  const { sizes: prodSizes } = useSelector(({ sizesReducer }) => sizesReducer);

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { id, action } = useParams();

  const isUpdate = action === 'update';
  const isCreate = action === 'add';
  const isDetail = action === 'detail';

  const [name, setName] = useState('');
  const [code, setCode] = useState('');
  const [color, setColor] = useState('');
  const [stock, setStock] = useState(0);
  const [price, setPrice] = useState(0);
  const [actualPrice, setActualPrice] = useState(0);
  const [sizes, setSizes] = useState([]);
  const [picture, setPicture] = useState('');
  const [isDisabled, setDisabled] = useState(true);
  const [open, setOpen] = useState(false);

  const [resPic, setResPic] = useState(def_img);

  useEffect(() => {
    if (id) dispatch(getOneData(id));
    else {
      setName('');
      setColor('');
      setStock(0);
      setSizes([]);
    }
  }, [id]);

  useEffect(() => {
    if (product?.id) dispatch(getSizesByProduct(product?.id));
  }, [product?.id]);

  useEffect(() => {
    if (product?.id && !isCreate) {
      setName(product.name);
      setCode(product.uuid);
      setColor(product.color);
      setStock(product.stock);
      setPrice(product.price);
      setActualPrice(product.actual_price);
      setPicture(product.picture);
    }
  }, [product]);

  useEffect(() => {
    if (product?.id) {
      setSizes(prodSizes);
    }
  }, [product, prodSizes]);

  const data = [
    {
      label: 'Product Code',
      value: code,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Product Name',
      value: name,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Color',
      value: color,
      isDisabled: isUpdate || isCreate ? false : true,
    },
    {
      label: 'Price',
      value: price,
      isDisabled: isUpdate || isCreate ? false : true,
      type: 'number',
    },
    {
      label: 'Actual Price',
      value: actualPrice,
      isDisabled: isUpdate || isCreate ? false : true,
      type: 'number',
    },
    {
      label: 'Stock',
      value: stock,
      isDisabled: true,
      type: 'number',
    },
  ];

  const handleChange = (value, i) => {
    if (i === 0) setCode(value);
    if (i === 1) setName(value);
    if (i === 2) setColor(value);
    if (i === 3) setPrice(value);
    if (i === 4) setActualPrice(value);
    if (i === 5) setStock(value);
  };

  const handleSubmit = async () => {
    let imgChecker = '';
    if (picture) imgChecker = picture.substring(0, 4);
    const payload = {
      uuid: code,
      name,
      color,
      // ...(resPic ? picture : {}),
      price: parseInt(price),
      actual_price: parseInt(actualPrice),
      stock: parseInt(stock),
    };
    if (imgChecker !== 'http') payload.picture = picture;
    if (!product?.id) {
      dispatch(createData(payload));
    } else {
      dispatch(updateData(product?.id, payload));
      history.push('/dashboard/products');
    }
  };

  const handleDelete = () => {
    dispatch(deleteData(id));
    history.push('/dashboard/products');
  };

  useEffect(() => {
    if (name && color && picture && price && actualPrice) setDisabled(false);
    else setDisabled(true);
  }, [name, color, picture, price, actualPrice]);

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setResPic(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
  });

  useEffect(() => {
    if (resPic.length > 0) {
      const file = resPic[0];
      getBase64(file)
        .then(result => {
          file['base64'] = result;
          setPicture(result);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }, [resPic]);

  useEffect(() => {
    if (sizes?.length) {
      let total = 0;
      for (const size of sizes) {
        total = total + size.stock;
      }
      setStock(total);
    }
  }, [sizes]);

  return (
    <>
      <CmtCard className="mb-3">
        <CmtCardHeader
          className="pt-4"
          title="Product Detail"
          titleProps={{
            variant: 'h4',
            component: 'div',
            className: classes.titleRoot,
          }}></CmtCardHeader>
        <CmtCardContent className={classes.cardContentRoot}>
          {product && (
            <Box display="flex">
              <Box
                display="flex"
                justifyContent="center"
                {...getRootProps()}
                className="pointer mb-5">
                <input {...getInputProps()} disabled={isDetail} />
                <img
                  height={240}
                  width={240}
                  style={{ maxHeight: '300px' }}
                  src={picture ? picture : resPic}
                  onError={() => {
                    setPicture(def_img);
                  }}
                />
              </Box>

              <Grid container spacing={5} className="ml-3">
                {data.map((row, i) => (
                  <Grid item lg={9} xl={5} key={i}>
                    <AppTextInput
                      label={row.label}
                      value={row.value}
                      onChange={e => handleChange(e.target.value, i)}
                      disabled={row.isDisabled}
                      type={row.type || 'text'}
                    />
                  </Grid>
                ))}
              </Grid>
            </Box>
          )}
        </CmtCardContent>
      </CmtCard>
      {product?.id && (
        <CmtCard>
          <CmtCardHeader
            className="pt-4"
            title="Product Sizes"
            titleProps={{
              variant: 'h4',
              component: 'div',
              className: classes.titleRoot,
            }}></CmtCardHeader>
          <CmtCardContent className={classes.cardContentRoot}>
            <Grid container spacing={3} className="sizes">
              {sizes &&
                sizes.map((row, i) => (
                  <Grid item xl={3} lg={6} key={i}>
                    <Size
                      name={name}
                      color={color}
                      data={row}
                      action={action}
                      id={product?.id}
                    />
                  </Grid>
                ))}
              {!isDetail && (
                <Grid item xl={3} lg={6}>
                  <Size action={action} add={true} id={product?.id} />
                </Grid>
              )}
            </Grid>
          </CmtCardContent>
        </CmtCard>
      )}
      <Box display="flex" justifyContent="flex-end" className="mt-3">
        {!isUpdate && (
          <Button
            color="default"
            variant="outlined"
            className="mr-3"
            onClick={() => history.push(`/dashboard/products`)}>
            <ArrowBack className="mr-2" /> Cancel
          </Button>
        )}
        {isUpdate && (
          <>
            <Button
              color="default"
              variant="outlined"
              className="mr-3"
              onClick={() => history.push(`/dashboard/products`)}>
              <ArrowBack className="mr-2" /> Cancel
            </Button>
            <Button color="primary" variant="contained" onClick={handleSubmit}>
              <Save className="mr-2" /> Save
            </Button>
          </>
        )}
        {isDetail && (
          <>
            <Button
              className="mr-3"
              color="secondary"
              variant="contained"
              onClick={() => setOpen(true)}>
              <Delete className="mr-2" /> Delete
            </Button>
            <Button
              color="primary"
              variant="contained"
              onClick={() => history.push(`/dashboard/products/update/${id}`)}>
              <Edit className="mr-2" /> Edit
            </Button>
          </>
        )}
        {isCreate && (
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
            disabled={isDisabled}>
            <Save className="mr-2" /> Save
          </Button>
        )}
      </Box>
      <Modal open={open} setOpen={setOpen} handleDelete={handleDelete} />
    </>
  );
};

export default RecentOrders;
