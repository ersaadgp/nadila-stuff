import React, { useRef, useState } from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Fade from '@material-ui/core/Fade';
import Modal from '@material-ui/core/Modal';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import { TextField } from '@material-ui/core';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  textAlign: 'center',
};

const btnStyle = {
  width: '100px',
  margin: '0 10px',
  zIndex: 99,
};

export default function BasicModal({ open, setOpen, setQty, qty }) {
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Modal open={open} onClose={handleClose}>
        <Fade in={open}>
          <Box sx={style}>
            <Box className="mt-3 d-flex">
              <TextField
                label="Print Quantity"
                onChange={e => setQty(e.target.value)}
                value={qty}
                size="small"
                variant="outlined"
                type="number"
              />
              <Button
                style={btnStyle}
                variant="contained"
                color="primary"
                onClick={handleClose}>
                Save
              </Button>
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
}
