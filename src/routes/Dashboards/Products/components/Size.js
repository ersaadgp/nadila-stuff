import React, { useState, useRef } from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import { useDispatch, useSelector } from 'react-redux';
import { Typography, Button, Box, Paper, Grid } from '@material-ui/core';
import {
  Add,
  Save,
  Delete,
  Edit,
  Print,
  FileCopy,
  Cancel,
} from '@material-ui/icons';
import AppTextInput from '@jumbo/components/Common/formElements/AppTextInput';
import Modal from '../components/ModalQR';
import { createSize, deleteSize, updateSize } from 'redux/actions/Sizes';
import ReactToPrint from 'react-to-print';
import QRCode from 'react-qr-code';

const useStyles = makeStyles(theme => ({
  content: {
    padding: 15,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  form: {
    padding: '10px 15px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}));

const Attachement = props => {
  const { data, action, add, name, color, id } = props;
  const componentRef = useRef();

  const classes = useStyles();
  const dispatch = useDispatch();
  // const { id } = useParams();
  const [stock, setStock] = useState(data?.stock || 0);
  const [size, setSize] = useState(data?.size || 0);
  const [open, setOpen] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [qty, setQty] = useState(0);

  const isDetail = action === 'detail';

  const handleAction = type => {
    const payload = { size: parseInt(size), stock: parseInt(stock) };
    if (type === 'create') {
      dispatch(createSize(id, payload));
    } else if (type === 'delete') {
      dispatch(deleteSize(data?.id));
    } else if (type === 'update') {
      dispatch(updateSize(data?.id, payload));
      setIsUpdate(false);
    }
  };

  const handleUpdate = () => {
    setIsUpdate(true);
  };

  const handleClick = e => {
    e.stopPropagation();
    setOpen(true);
  };

  return (
    <>
      <Paper variant="outlined" className={'p-list'}>
        <Box className={add || isUpdate ? classes.form : classes.content}>
          {add || isUpdate ? (
            <Grid container spacing={3}>
              <Grid item xs={4}>
                <AppTextInput
                  label="Size"
                  type="number"
                  value={size}
                  onChange={e => setSize(e.target.value)}
                />
              </Grid>
              <Grid item xs={4}>
                <AppTextInput
                  value={stock}
                  label="Stock"
                  onChange={e => setStock(e.target.value)}
                  type="number"
                />
              </Grid>
              <Grid item xs={4}>
                {isUpdate ? (
                  <>
                    <Button
                      disabled={!size || !stock}
                      onClick={() => handleAction('update')}
                      variant="contained"
                      color="primary"
                      className="mt-3 ml-3 mr-1 btn-size">
                      <Save />
                    </Button>
                    <Button
                      onClick={() => setIsUpdate(false)}
                      variant="outlined"
                      color="secondary"
                      className="mt-3 btn-size">
                      <Cancel />
                    </Button>
                  </>
                ) : (
                  <Button
                    disabled={!size || !stock}
                    onClick={() => handleAction('create')}
                    variant="outlined"
                    color="primary"
                    className="mt-3 btn-size">
                    <Add />
                  </Button>
                )}
              </Grid>
            </Grid>
          ) : (
            <>
              <Typography variant="body2">
                {open ? 'Loading' : `Size ${data?.size}`}
              </Typography>
              <Typography variant="body2">{data?.stock} Pc(s)</Typography>
              {isDetail && (
                <div>
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={handleClick}
                    className="btn-size mr-1">
                    <FileCopy />
                  </Button>
                  <ReactToPrint
                    trigger={() => (
                      <Button
                        variant="contained"
                        color="primary"
                        className="btn-size"
                        disabled={qty < 1}>
                        <Print />
                      </Button>
                    )}
                    content={e => componentRef.current}
                    onAfterPrint={() => setOpen(false)}
                  />
                </div>
              )}
              {!isDetail && (
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    className="btn-size"
                    disabled={isDetail}
                    onClick={handleUpdate}>
                    <Edit />
                  </Button>
                  <Button
                    disabled={isDetail}
                    variant="outlined"
                    color="secondary"
                    className="btn-size ml-1"
                    onClick={() => handleAction('delete')}>
                    <Delete />
                  </Button>
                </div>
              )}
            </>
          )}
        </Box>
      </Paper>
      <Modal
        setQty={setQty}
        qty={qty}
        open={open}
        setOpen={setOpen}
        data={data}
        name={name}
        color={color}
      />
      <div className="print">
        <div ref={componentRef}>
          {Array(parseInt(qty || 0))
            .fill()
            .map(row => (
              <div key={row} className="qr-print">
                <QRCode value={`${data?.id}`} />
                <p>{name}</p>
                <p>{`${color} (${data?.size})`}</p>
              </div>
            ))}
        </div>
      </div>
    </>
  );
};

export default Attachement;
