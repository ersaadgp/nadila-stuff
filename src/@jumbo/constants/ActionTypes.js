export const SHOW_MESSAGE = 'show_message';
export const HIDE_MESSAGE = 'hide_message';
export const FETCH_START = 'fetch_start';
export const FETCH_SUCCESS = 'fetch_success';
export const FETCH_ERROR = 'fetch_error';

export const UPDATE_AUTH_USER = 'update_auth_user';
export const UPDATE_LOAD_USER = 'update_load_user';
export const SEND_FORGET_PASSWORD_EMAIL = 'send_forget_password_email';
export const SIGNIN_GOOGLE_USER_SUCCESS = 'signin_google_user_success';
export const SIGNIN_FACEBOOK_USER_SUCCESS = 'signin_facebook_user_success';
export const SIGNIN_TWITTER_USER_SUCCESS = 'signin_twitter_user_success';
export const SIGNIN_GITHUB_USER_SUCCESS = 'signin_github_user_SUCCESS';
export const SIGNIN_USER_SUCCESS = 'signin_user_success';
export const SIGNOUT_USER_SUCCESS = 'signout_user_success';
export const PASSWORD_CHANGED = 'password_changed';

// Products
export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_PRODUCT = 'GET_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CLEAR_PRODUCT = 'CLEAR_PRODUCT';
export const GET_PRODUCTS_COMBO = 'GET_PRODUCTS_COMBO';

// Sizes
export const GET_SIZES = 'GET_SIZES';
export const GET_SIZE = 'GET_SIZE';
export const UPDATE_SIZE = 'UPDATE_SIZE';
export const CREATE_SIZE = 'CREATE_SIZE';
export const DELETE_SIZE = 'DELETE_SIZE';
export const GET_SIZES_COMBO = 'GET_SIZES_COMBO';

// Tx
export const GET_TRANSACTIONS = 'GET_TRANSACTIONS';

// Customers
export const GET_CUSTOMERS = 'GET_CUSTOMERS';
export const GET_CUSTOMER = 'GET_CUSTOMER';
export const UPDATE_CUSTOMER = 'UPDATE_CUSTOMER';
export const CREATE_CUSTOMER = 'CREATE_CUSTOMER';
export const DELETE_CUSTOMER = 'DELETE_CUSTOMER';
export const CLEAR_CUSTOMER = 'CLEAR_CUSTOMER';
export const GET_PRODUCT_CUST = 'GET_PRODUCT_CUST';
export const DELETE_PRODUCT_CUST = 'DELETE_PRODUCT_CUST';
export const UPDATE_PRODUCT_CUST = 'UPDATE_PRODUCT_CUST';
export const CREATE_PRODUCT_CUST = 'CREATE_PRODUCT_CUST';
