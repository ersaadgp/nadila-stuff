import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Collapse, IconButton, Box } from '@material-ui/core';
import { alpha } from '@material-ui/core/styles';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import { Alert } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';

import CmtImage from '../../../../@coremat/CmtImage';

import IntlMessages from '../../../utils/IntlMessages';
import { AuhMethods } from '../../../../services/auth';
import ContentLoader from '../../ContentLoader';
import { CurrentAuthMethod } from '../../../constants/AppConstants';
import AuthWrapper from './AuthWrapper';
import { setPasswordChanged } from '../../../../redux/actions/Auth';

const useStyles = makeStyles(theme => ({
  authThumb: {
    backgroundColor: alpha(theme.palette.primary.main, 0.12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    [theme.breakpoints.up('md')]: {
      width: '50%',
      order: 2,
    },
  },
  authContent: {
    padding: 30,
    [theme.breakpoints.up('md')]: {
      order: 1,
      width: props => (props.variant === 'default' ? '50%' : '100%'),
    },
    [theme.breakpoints.up('xl')]: {
      padding: 50,
    },
  },
  titleRoot: {
    marginBottom: 14,
    color: theme.palette.text.primary,
  },
  textFieldRoot: {
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: alpha(theme.palette.common.dark, 0.12),
    },
  },
  alertRoot: {
    marginBottom: 10,
  },
}));

//variant = 'default', 'standard', 'bgColor'
const ForgotPassword = ({
  method = CurrentAuthMethod,
  variant = 'default',
  wrapperVariant = 'default',
}) => {
  const { password_changed } = useSelector(({ auth }) => auth);

  const [open, setOpen] = React.useState(false);
  const [currentPassword, setCurrentPassword] = useState();
  const [newPassword, setNewPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const [error, setError] = useState(false);
  const dispatch = useDispatch();
  const classes = useStyles({ variant });
  const history = useHistory();

  useEffect(() => {
    let timeOutStopper = null;
    if (password_changed) {
      setOpen(true);

      timeOutStopper = setTimeout(() => {
        dispatch(setPasswordChanged(false));
        history.push('/dashboard/users');
      }, 1500);
    }

    return () => {
      if (timeOutStopper) clearTimeout(timeOutStopper);
    };
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [password_changed]);

  const onSubmit = () => {
    let valid = true;
    setError(false);
    if (!currentPassword || !newPassword || !confirmPassword) {
      valid = false;
      setError(true);
    }

    if (newPassword === confirmPassword && valid) {
      dispatch(AuhMethods[method].onChangePassword({ password: newPassword }));
    }
  };

  return (
    <AuthWrapper variant={wrapperVariant}>
      {variant === 'default' ? (
        <div className={classes.authThumb}>
          <CmtImage src={'/images/auth/forgot-img.png'} />
        </div>
      ) : null}
      <div className={classes.authContent}>
        <div className={'mb-7'}>
          <CmtImage src={'/images/logo.png'} />
        </div>
        <Typography component="div" variant="h1" className={classes.titleRoot}>
          Ganti Password Admin
        </Typography>
        <Collapse in={open}>
          <Alert
            variant="outlined"
            severity="success"
            className={classes.alertRoot}
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}>
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }>
            Password berhasil diganti.
          </Alert>
        </Collapse>
        {error && (
          <Alert
            variant="outlined"
            severity="error"
            className={classes.alertRoot}
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setError(false);
                }}>
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }>
            Semua form wajib diisi.
          </Alert>
        )}
        <form>
          <div className={'mb-5'}>
            <TextField
              label={<IntlMessages id="appModule.currentPassword" />}
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
              onChange={e => setCurrentPassword(e.target.value)}
            />
            <TextField
              label={<IntlMessages id="appModule.newPassword" />}
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
              onChange={e => setNewPassword(e.target.value)}
            />
            <TextField
              label="Confirm Password"
              fullWidth
              margin="normal"
              variant="outlined"
              className={classes.textFieldRoot}
              onChange={e => setConfirmPassword(e.target.value)}
            />
          </div>
          <Box className="mb-5" display="flex" justifyContent="center">
            <Button
              onClick={() => window.history.back()}
              variant="contained"
              style={{
                color: '#FB7880',
                border: '1px solid #FB7880',
                backgroundColor: '#FCFDFE',
                margin: '0 1rem',
                boxShadow: 'none',
              }}>
              Batal
            </Button>
            <Button
              onClick={onSubmit}
              variant="contained"
              style={{
                backgroundColor: '#FB7880',
                margin: '0 1rem',
                color: '#FCFDFE',
                boxShadow: 'none',
              }}>
              <IntlMessages id="appModule.changePassword" />
            </Button>
          </Box>
        </form>
        <ContentLoader />
      </div>
    </AuthWrapper>
  );
};

export default ForgotPassword;
