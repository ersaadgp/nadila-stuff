import IntlMessages from '../../../utils/IntlMessages';
import { Category, ShoppingCart, History } from '@material-ui/icons';
import React from 'react';

const role = localStorage.getItem('role');
const isAdmin = role === 'admin';

const dashboard = [
  {
    name: 'Products',
    icon: <Category />,
    type: 'item',
    link: '/dashboard/products',
  },
  {
    name: 'Customers',
    icon: <ShoppingCart />,
    type: 'item',
    link: '/dashboard/customers',
  },
  {
    name: 'Records',
    icon: <History />,
    type: 'item',
    link: '/dashboard/records',
  },
];

const dashboardAdmin = [
  {
    name: 'Products',
    icon: <Category />,
    type: 'item',
    link: '/dashboard/products',
  },
  {
    name: 'Customers',
    icon: <ShoppingCart />,
    type: 'item',
    link: '/dashboard/customers',
  },
];

export const sidebarNavs = [
  {
    name: <IntlMessages id={'sidebar.dashboard'} />,
    type: 'section',
    children: isAdmin ? dashboardAdmin : dashboard,
  },
];

export const horizontalDefaultNavs = [
  {
    name: <IntlMessages id={'sidebar.dashboard'} />,
    type: 'section',
    children: isAdmin ? dashboardAdmin : dashboard,
  },
];

export const minimalHorizontalMenus = [
  {
    name: <IntlMessages id={'sidebar.dashboard'} />,
    type: 'section',
    children: isAdmin ? dashboardAdmin : dashboard,
  },
];
