import { fetchError, fetchStart, fetchSuccess } from '../../../redux/actions';
import {
  setAuthUser,
  setForgetPassMailSent,
  updateLoadUser,
  setPasswordChanged,
} from '../../../redux/actions/Auth';
import React from 'react';
import axios from './config';
const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem('token');

const JWTAuth = {
  onRegister: ({ name, email, password }) => {
    return dispatch => {
      dispatch(fetchStart());
      axios
        .post(URL + '/api/register', {
          password: password,
          username: name,
        })
        .then(({ data }) => {
          if (data.success) {
            dispatch(fetchSuccess('Account Register Success'));
          } else {
            dispatch(fetchError(data.error));
          }
        })
        .catch(function(error) {
          dispatch(fetchError(error.message));
        });
    };
  },

  onLogin: ({ email, password }) => {
    return dispatch => {
      try {
        dispatch(fetchStart());
        axios
          .post(URL + '/api/login', {
            username: email,
            password: password,
          })
          .then(({ data }) => {
            const res = data.data;
            if (data.data) {
              localStorage.setItem('token', res.token);
              localStorage.setItem('role', res.role);
              axios.defaults.headers.common['Authorization'] =
                'Bearer ' + res.token;
              dispatch(fetchSuccess());
              dispatch(setAuthUser(res));
              window.location.reload();
              // dispatch(JWTAuth.getAuthUser(true, res.token));
            } else {
              dispatch(fetchError(data.error));
            }
          })
          .catch(function(error) {
            dispatch(fetchError(error.message));
          });
      } catch (error) {
        dispatch(fetchError(error.message));
      }
    };
  },

  onLogout: token => {
    return dispatch => {
      dispatch(fetchStart());
      axios
        .get(URL + '/user/logout', {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then(data => {
          if (data.status == 200) {
            dispatch(fetchSuccess());
            dispatch(setAuthUser(null));
          } else {
            dispatch(fetchError(data.error));
          }
        })
        .catch(function(error) {
          dispatch(fetchError(error.message));
        });
    };
  },

  onChangePassword: password => {
    return dispatch => {
      dispatch(fetchStart());
      axios
        .put(URL + '/user/password', password, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then(({ data }) => {
          if (data.success) {
            setTimeout(() => {
              dispatch(setPasswordChanged(true));
              dispatch(fetchSuccess());
            }, 300);
          } else {
            dispatch(fetchError(data.error));
          }
        })
        .catch(function(error) {
          dispatch(fetchError(error.message));
        });
    };
  },

  // getAuthUser: (loaded = false, token) => {
  //   return dispatch => {
  //     if (!token) {
  //       const token = localStorage.getItem('token');
  //       axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  //     }
  //     dispatch(fetchStart());
  //     dispatch(updateLoadUser(loaded));
  //     axios
  //       .post('auth/me')
  //       .then(({ data }) => {
  //         if (data.result) {
  //           dispatch(fetchSuccess());
  //           dispatch(setAuthUser(data.user));
  //         } else {
  //           dispatch(updateLoadUser(true));
  //         }
  //       })
  //       .catch(function(error) {
  //         dispatch(updateLoadUser(true));
  //       });
  //   };
  // },

  onForgotPassword: () => {
    return dispatch => {
      dispatch(fetchStart());

      setTimeout(() => {
        dispatch(setForgetPassMailSent(true));
        dispatch(fetchSuccess());
      }, 300);
    };
  },
  getSocialMediaIcons: () => {
    return <React.Fragment> </React.Fragment>;
  },
};

export default JWTAuth;
